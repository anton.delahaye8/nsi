# Spécialité Numérique et Sciences Informatiques

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------

## Présentation

Supports de cours, TD et TP pour la spécialité NSI.

* [Classe de première](./nsi_premiere/index_premiere.md)
* [Classe de terminale](./nsi_terminale/index_terminale.md)

Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/nsi)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/nsi)

D'autres collègues ont mis en ligne des cours détaillés et fournis en activités :

* [Pixees](https://pixees.fr/informatiquelycee/) : informatique au lycée
* [Lycée Blaise Pascal de Clermont Ferrand](https://info.blaisepascal.fr/) : l'informatique, c'est fantastique !
* [Lycée Angellier de Dunkerque](http://vfsilesieux.free.fr/)
* [Lycée Jean Moulin de Dradignan](https://isn-icn-ljm.pagesperso-orange.fr/)
* [Infoforall](https://www.infoforall.fr/) : l'informatique pour tous


## Programmes de la NSI

* Programme de première NSI : [pdf](./nsi_premiere/Programme_1NSI.pdf)
* Programme de terminale NSI : [pdf](./nsi_terminale/Programme_TNSI.pdf)


## Le baccalauréat NSI

* Résumé des épreuves en première et en termnale : [pdf](./Bac_NSI.pdf)
* Calendrier des épreuves en première et en termnale : [pdf](./Calendrier_epreuves.pdf)
* Coefficients des épreuves en première et en termnale : [pdf](./Coefficients_epreuves.pdf)

* Banque nationale des sujets de première : [lien](https://www.sujetdebac.fr/annales/sujets-e3c/spe-numerique-informatique/premiere/2020/)
* Sujet 0 de terminale : [pdf](./S0BAC21-Tle-SPE-NSI.pdf)


## Outils pour la NSI

* Logiciels et modules python : [md](./outils/outils.md)

## Bibliographie

* [Wikipedia](https://fr.wikipedia.org)
* [Openclassrooms](https://openclassrooms.com/fr/)
* [Université de Lille - Formation à l'enseignement de l'informatique](http://fe.fil.univ-lille1.fr)
* [SUPINFO International University](https://www.supinfo.com/)
* Ellipse : Spécialité Numérique et sciences informatiques : 30 leçons avec exercices corrigés - Première - Nouveaux programmes
* Ellipse : Spécialité Numérique et sciences informatiques : 24 leçons avec exercices corrigés - Terminale - Nouveaux programmes
* Nathan : Interros des Lycées Numérique Sciences Informatiques - Terminale

--------------------
