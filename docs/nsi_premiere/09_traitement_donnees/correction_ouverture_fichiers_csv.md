# Stockage des données
# Les fichiers CSV

# Correction

1\. Utiliser un éditeur de texte simple.

2\. Copier-coller.

3\. Bien préciser l'extension de fichier `.csv` et l'encodage UTF-8.

4\. Le tableur reconnaît qu'il importe un fichier csv et demande notamment quel est le séparateur utilisé. Ici il faut préciser la virgule `,`.

5\. Il faut faire attention à l'extension du fichier, le séparateur et l'encodage.

6\. Les deux lignes ont bien été ajoutées.

7\. On crée le fichier Python.

8\. Soit les lignes de code ci-dessous.

```python
1|  fichier = open('ma_table.csv', 'r', encoding='utf-8')   	
2|  ma_table = fichier.readlines()   	
3|  fichier.close()   	
4|  
5|  print(ma_table)   
```

* Ligne 1 : le fichier est ouvert en lecture seule `'r'` avec l'encodage `'utf-8'`.
* Ligne 2 : toutes les lignes du fichier sont lues en une seule fois et stockées sous forme de liste dans la variable `ma_table`.
* Ligne 3 : fermeture du fichier.
* Ligne 5 : affichage du résultat.

9\. On obtient la liste des lignes du fichier csv. Chaque ligne est une chaîne de caractères contenant les données séparées par des virgules. La première ligne correspond aux descripteurs de la collection. Les autres sont les objets.


`'\n'`correspond au caractère non imprimable retour à la ligne.

10\. La première ligne permet de supprimer le `'\n'`. Elle est écrite par compréhension.

11\. La méthode `.split(',')` permet de découper les chaînes de caractères selon le séparateur `','`. On obtient une liste 2D dont chaque sous-liste correspond à une ligne du fichier csv.

12\. La ligne d'indice 0 correspond aux descriteurs. Donc la ligne d'indice 1 correspond au premier objet et la ligne d'indice 2 au deuxième. Pour l'extraire il suffit d'écrire :

```python
1|  objet = ma_table[2]
2|  print(objet)
```

13\. Le descripteur **Prénom** correspond à la colonne 2, d'indice 1, de chaque ligne. On extrait sa valeur par une double indexation. Par exemple pour le deuxième objet :

```python
1|  prenom = ma_table[2][1]
2|  print(prenom)
```

14\. On extrait la ligne d'indice 0 de la table.

```python
1|  descripteurs = ma_table[0]
2|  print(descripteurs)
```

15\. On extrait toutes les autres lignes.

```python
1|  objets = ma_table[1:]
2|  print(objets)
```

16\. En ouvrant le fichier avec un éditeur de texte ou un tableur on vérifie l'ajout de la ligne.

17\. On réitère les lignes de code de la question 16 avec deux `.write()` dont on change le contenu.

18\. On teste les lignes de code.

19\. On extrait le troisième élément de la variable `objet` avec `objet[2]`.

20\. On teste les lignes de code.

21\.  On réitère les lignes de code de la question 20 avec deux `.writerow()` dont on change le contenu.

22\. Ne pas oublier que la table peut-être lue en une seule fois avec :

```python
 1|  import csv
 2|  
 3|  fichier = open('ma_table.csv', 'r', newline='', encoding='utf-8')
 4|  ma_table = csv.reader(fichier, delimiter=',')
 5|  
 6|  ma_table = [ligne for ligne in ma_table]
 7|  
 8|  fichier.close()
 9|  
10|  print(ma_table)
```
