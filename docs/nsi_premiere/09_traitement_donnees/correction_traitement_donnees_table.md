# Traitement des données en table
# sur la base de fichiers CSV

# Correction

## I. Ouverture du fichier

1\. Créer le fichier Python.

2\. On écrit le code ci-dessous.

```python
1|  import csv
2|  
3|  fichier = open('pokemon_stats_1.csv', 'r', newline='', encoding='utf-8')
4|  table_pokemon_1 = csv.reader(fichier, delimiter=';')
5|  
6|  table_pokemon_1 = [ligne for ligne in table_pokemon_1]
7|  
8|  fichier.close()
```

* Ligne 3 : ouverture du fichier.
* Ligne 4 : lecture du fichier.
* Ligne 6 : conversion de la table sous forme de liste 2D, une ligne étant une liste.

3\. C'est une collection de Pokémon. Les descripteurs correspondent à la première ligne de la liste `table_pokemon_1` : `['name', 'classification', 'attack', 'defense']`.

4\. Il suffit d'extraire la ligne d'indice 0 pour obtenir les descripteurs. Les objets correspondent aux autres lignes.

```python
1|  descripteurs_pokemon_1 = table_pokemon_1[0]
2|  objets_pokemon_1 = table_pokemon_1[1:]
```

5\. On utilise la fonction `len()`. Il y a 507 Pokemons dans la collection.

```python
print('Il y a', len(objets_pokemon_1), 'Pokemons dans la collection.')
```


## II. Recherches dans une table

1\. Puisque `objets_pokemon_1` ne contient que les Pokémons, le Pokemon 341 s'obtient par :

```python
print(objets_pokemon_1[341])
```

Plus spécifiquement on peut extraire séparément son nom et son attaque :

```python
1|  print(objets_pokemon_1[341][0])
2|  print(objets_pokemon_1[341][2])
```

2\. En utilisant `print()`, l'indice trouvé est 426. C'est donc le $`\mathrm{427^{ème}}`$.

3\. La ligne de code ci-dessous créé une liste par compréhension.

```python
indices_objets = [i for i in range(len(objets_pokemon_1)) if objets_pokemon_1[i][0] == 'Toxicroak']
```

La boucle `for` balaie tous les indices de la table `objets_pokemon_1` en partant de 0. Si le nom du Pokémon obtenu par l'extraction `objets_pokemon_1[i][0]` correspond à `'Toxicroak'`, l'indice `i` est retenu dans la liste `indices_objets`.

4\. Il suffit d'afficher le nom de l'objet 426 avec `print(objets_pokemon_1[426][0])`.

5\. On utilise le code de la question 2 en remplaçant `'Toxicroak'` par `'Lairon'`. On obtient l'indice 218. `print(objets_pokemon_1[218][0])` permet de vérifier la réponse.

6\. On utilise toujours la liste par compréhension mais avec le test `if objets_pokemon_1[i][1] == 'Dragon Pokémon'`. On obtient une liste d'indice : `[183, 243, 300, 484]`. Il y a donc 4 Pokémons classés `'Dragon Pokémon'`. On extrait leur nom comme pour les questions précédentes, pour chaque indice. Ce sont Dratini, Goodra, Horsea et Dragonite.

7\. La boucle par compréhenion est donnée ci-dessous.

```python
1|  indices_objets = [12, 74, 123, 253, 388]
2|  liste_objets = [objets_pokemon_1[indice] for indice in indices_objets]
3|  print(liste_objets)
```

On obtient :

`[['Dialga', 'Temporal Pokémon', '120', '120'], ['Fletchling', 'Tiny Robin Pokémon', '50', '43'], ['Manaphy', 'Seafaring Pokémon', '100', '100'], ['Magcargo', 'Lava Pokémon', '50', '120'], ['Makuhita', 'Guts Pokémon', '60', '30']]`

8\. On change la condition lors de la création de la liste par compréhension. Attention à convertir les chaînes de caractères en entiers ou réels.

```python
1|  indices_objets = [i for i in range(len(objets_pokemon_1)) if int(objets_pokemon_1[i][2]) >=70]
2|  print(len(indices_objets))
```

Il y a 284 Pokémons avec une attaque supérieure ou égale à 70.

9\. La condition devient `if int(objets_pokemon_1[i][2]) > 100 and int(objets_pokemon_1[i][3]) > 100`. On obtient 33 Pokémons qui ont une attaque et une défense strictements supérieures ou égales à 100.

## III. Premiers calculs dans une table

1\. Le code classique est le suivant :

```python
1|  somme = 0
2|  for el in objets_pokemon_1:
3|      somme = somme + int(el[2])
4|  moyenne = somme / len(objets_pokemon_1)
5|  
6|  print("L'attaque moyenne des Pokémons est de", moyenne)
```

2\. Pour le calcul de la défense moyenne il suffit de remplacer `int(el[2])` par `int(el[3])`.

3\. L'écriture en Python de l'algorithme est la suivante.

```python
1|  valeur_max = int(objets_pokemon_1[0][2])
2|  indice_max = 0
3|  for i in range(1, len(objets_pokemon_1)):
4|      if int(objets_pokemon_1[i][2]) > valeur_max:
5|          valeur_max = int(objets_pokemon_1[i][2])
6|          indice_max = i
7|  
8|  print('indice :', indice_max, ', valeur :', valeur_max, ', nom :', objets_pokemon_1[indice_max][0])
```

On obtient : `indice : 41 , valeur : 185 , nom : Heracross`.

4\. On replace tous les `max` par des `min` dans le code précédent. On obient : `indice : 103 , valeur : 5 , nom : Happiny`.

5\. Il suffit de remplacer tous les `objets_pokemon_1[0][2]` par `objets_pokemon_1[0][3]` dans les deux codes précédents.

Pour la défense maximale on obtient : `indice : 17 , valeur : 230 , nom : Aggron`.

Pour la défense minimale on obtient : `indice : 103 , valeur : 5 , nom : Happiny`.

6\. On adapte le code avec le calcul de la valeur moyenne attaque-défense.

```python
1|  valeur_max = (int(objets_pokemon_1[0][2]) + int(objets_pokemon_1[0][3]))/2
2|  indice_max = 0
3|  for i in range(1, len(objets_pokemon_1)):
4|      moyenne = (int(objets_pokemon_1[i][2]) + int(objets_pokemon_1[i][3]))/2
5|      if moyenne > valeur_max:
6|          valeur_max = moyenne
7|          indice_max = i
8|  
9|  print('indice :', indice_max, ', valeur :', valeur_max, ', nom :', objets_pokemon_1[indice_max][0])
```

Le Pokémon qui a la meilleure moyenne attaque-défense est donc Aggron avec 140 en attaque et 230 en défense.


## IV. Tris dans une table

1\. On obtient la liste des objets de la table triée selon le premier descripteur, c'est-à-dire le nom, par ordre croissant.

2\. On adapte l'exemple. Noter l'utilisation de `reverse=True`.

```python
1|  def fonction_cle_de_tri(liste):
2|      return int(liste[2])
3|  
4|  liste_triee = sorted(objets_pokemon_1, key=fonction_cle_de_tri, reverse=True)
5|  print(liste_triee)
```

3\. On trie cette fois-ci par ordre décroissant de la défense en remplaçant `liste[2]` par `liste[3]`. Les trois premiers objets de la liste triée donnent les trois meilleurs défenseurs. Ce sont Aggron, Steelix et Shuckle. Aggron et Steelix ont des attaques supérieures à 100. Celle de Shuckle n'est que de 10.

4\. Les noms, les valeurs des attaques ou défenses sont des données puisqu'elles ne sont pas interprétées. Affirmer que Aggron, Steelix et Shuckle sont les trois meilleurs défenseurs est une information puisque c'est une interprétation de la donnée défense.

5\. On adapte la fonction de trie avec le calcul de la moyenne attaque-défense.

```python
1|  def fonction_cle_de_tri(liste):
2|      return (int(liste[2])+int(liste[3]))/2
```

Les trois meilleurs sont alors Aggron, Steelix et Groudon. Aggron et Steelix sont aussi les meilleurs défenseurs mais pas les meilleurs attaquants. Groudon est quant à lui le $`\mathrm{3^{ème}}`$ meilleur attaquant.


## V. Fusion de tables ayant les mêmes descripteurs, recherche des doublons

1\. Voici le code permettant l'importation des deux tables.

```python
 1|  import csv
 2|  
 3|  fichier = open('pokemon_stats_2_a.csv', 'r', newline='', encoding='utf-8')
 4|  table_pokemon_2_a = csv.reader(fichier, delimiter=';')
 5|  table_pokemon_2_a = [ligne for ligne in table_pokemon_2_a]
 6|  fichier.close()
 7|  
 8|  fichier = open('pokemon_stats_2_b.csv', 'r', newline='', encoding='utf-8')
 9|  table_pokemon_2_b = csv.reader(fichier, delimiter=';')
10|  table_pokemon_2_b = [ligne for ligne in table_pokemon_2_b]
11|  fichier.close()
```

2\. Qui est complété ci-dessous pour séparer descripteurs et objets.

```python
12|  
13|  descripteurs_pokemon_2_a = table_pokemon_2_a[0]
14|  objets_pokemon_2_a = table_pokemon_2_a[1:]
15|  
16|  descripteurs_pokemon_2_b = table_pokemon_2_b[0]
17|  objets_pokemon_2_b = table_pokemon_2_b[1:]
```

3\. On affiche les descripteurs de chaque table.

```python
1|  print(descripteurs_pokemon_2_a)
2|  print(descripteurs_pokemon_2_b)
```

Les descritpeurs sont bien communs, à savoir : `['name', 'capture_rate', 'height_m', 'weight_kg', 'classification', 'type']`.

4\. Le résultat de la question précédente montre aussi que les descripteurs sont dans le même ordre pour les deux tables.

5\. Il suffit d'utiliser l'opérateur de concaténation `+`.

```python
objets_pokemon_2 = objets_pokemon_2_a + objets_pokemon_2_b
```

6\. On affiche les longueurs des trois listes `objets_pokemon_2_a`, `objets_pokemon_2_b` et `objets_pokemon_2`. On obtient respectivement 243, 294
et 537. On a bien 537 = 243 + 294.

7\. L'algorithme traduit en Python est :

```python
1|  doublons = False
2|  i = 0
3|  while doublons != True and i < len(objets_pokemon_2):
4|      for j in range(i + 1, len(objets_pokemon_2)):
5|          if objets_pokemon_2[j] == objets_pokemon_2[i]:
6|              doublons = True
7|      i = i + 1
8|     
9|  print(doublons)
```

En l'appliquant, `doublons` est passé à `True` ce qui signifie qu'il y a au moins un doublon.

8\. On peut ajouter un `print(i, j)` entre la ligne 6 et la ligne 7 du code précédent, dans le bloc d'instruction du `if`. Il permet d'obtenir les indices du Pokémon trouvé en double : 4 et 327. Ils correspondent tous deux au Pokémon Spheal. On le vérifie avec `print(objets_pokemon_2[4][0], objets_pokemon_2[327][0])`.

9\.  L'algorithme traduit en Python est :

```python
1|  objets_pokemon_2_sans_doublons = []
2|  for objet in objets_pokemon_2:
3|      if objet not in objets_pokemon_2_sans_doublons:
4|          objets_pokemon_2_sans_doublons.append(objet)
5|  
6|  print(len(objets_pokemon_2_sans_doublons))
```

On obtient bien 507 objets. Comme il y en avait 537 au départ, il y avait donc 30 doublons.

10\. L'exportation se fait de la manière suivante.

```python
1|  fichier = open('pokemon_stats_2.csv', 'w', newline='', encoding='utf-8')
2|  ma_table = csv.writer(fichier, delimiter=';')
3|  
4|  ma_table.writerows(objets_pokemon_2)
5|      
6|  fichier.close()
```


## VI. Fusion de tables ayant des descripteurs différents

1\. On ouvre les deux fichiers dans un tableur en faisant bien attention au séparateur.

2\. Deux descripteurs sont communs : `name` et	`classification`.

3\. Le descripteur `name` permet d'identifier de manière unique un objet dans les deux tables. C'est donc la clé primaire.

4\. Il faut trier les deux tables selon leur clé primaire. Bien vérifier quelles ont le même nombre d'objets et que chaque ligne correspond au même objet dans chaque table. On peut alors copier les colonnes manquantes d'une des tables dans l'autre.

5\. On exporte le résultat sous forme de fichier CSV en UTF-8.


## VII. Test de cohérence d’une table, domaines de valeurs

1\. Les domaines de valeurs sont les suivants :

* hauteur : réel positif non nul.
* type : chaîne de caractères, un seul mot, en minuscules.
* classification : chaîne de caractères, Majuscule à la première lettre de chaque mot, sinon minuscules.

2\. L'importation de la collection se fait avec le code ci-dessous.

```python
 1|  import csv
 2|  
 3|  fichier = open('pokemon_stats_4.csv', 'r', newline='', encoding='utf-8')
 4|  table_pokemon_4 = csv.reader(fichier, delimiter=';')
 5|  
 6|  table_pokemon_4 = [ligne for ligne in table_pokemon_4]
 7|  
 8|  fichier.close()
 9|  
10|  descripteurs_pokemon_4 = table_pokemon_4[0]
11|  
12|  objets_pokemon_4 = table_pokemon_4[1:]
```

3\. On peut balayer les valeurs du descriteur hauteur et tester s'il existe des rééls inférieurs ou égaux à 0.

```python
1|  for objet in objets_pokemon_4:
2|      valeur = float(objet[6])
3|      if valeur <= 0:
4|          print('''Collection non cohérente pour l'objet :''', objet)
```

4\. Le plus simple est de supprimer les éléments non cohérents avec le mot clé `del`. On commencera par identifier les indices des éléments non cohérents pour un descripteur donné.

```python
 1|  liste_indices_non_coherents = []
 2|  
 3|  for i in range(len(objets_pokemon_4)):
 4|      valeur = float(objets_pokemon_4[i][6])
 5|      if valeur <= 0:
 6|          liste_indices_non_coherents.append(i)
 7|  
 8|  print(liste_indices_non_coherents)
 9|  
10|  liste_indices_non_coherents.reverse()
11|  
12|  for el in liste_indices_non_coherents:
13|      del objets_pokemon_4[el]
14|  
15|  liste_indices_non_coherents = []
16|  
17|  for i in range(len(objets_pokemon_4)):
18|      valeur = float(objets_pokemon_4[i][6])
19|      if valeur <= 0:
20|          liste_indices_non_coherents.append(i)
21|  
22|  print(liste_indices_non_coherents)
```

* Lignes de 1 à 8 : identification des objets non cohérents pour le descripteur hauteur.
* Lignes de 10 à 13 : suppression des éléments non cohérents. Il faut inverser la liste des indices pour commencer par le dernier.
* Lignes de 15 à 22 : vérification de la cohérence en relançant une identification des objets non cohérents. On doit obtenir maintenant une liste vide.
