# HTML et jeu de caractères

# Correction

3\. Les caractères accentués ne s'affichent pas correctement. En effet dans le code HTML il est écrit `charset="ascii"`. Ce jeu de caractères ne supporte pas les caractères accentués.

5\. Avec `charset="utf-8"` les caractères accentués s'affichent correctement. En effet ce jeu de caractères contient les caractères accentués et beaucoup d'autres symboles.
