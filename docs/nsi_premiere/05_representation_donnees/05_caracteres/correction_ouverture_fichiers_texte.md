# Fichiers texte

# Correction

1\. On utilise un éditeur très simple qui permet d'enregistrer des fichiers au format texte pur `.txt`.

2\. Copier-coller le texte.

3\. A l'enregistrement bien faire attention au format `.txt` et à l'encodage UTF-8.

4\. On crée le fichier Python.

5\. Soit les lignes de code ci-dessous.

```python
1|  fichier = open('mon_texte.txt', 'r', encoding='utf-8')
2|  ligne = fichier.readline()
3|  print(ligne)
4|  fichier.close()
```

* Ligne 1 : le fichier `'mon_texte.txt'` est ouvert en lecture `'r'` en utilisant l'encodage `'utf-8'`.
* Ligne 2 : lecture d'une ligne du fichier (la première). La ligne est stockée dans la variable `'ligne'`.
* Ligne 3 : affichage de la ligne.
* Ligne 4 : fermeture du fichier.

6\. On obtient le code :

```python
1|  fichier = open('mon_texte.txt', 'r', encoding='utf-8')
2|  ligne = fichier.readline()
3|  print(ligne)
4|  ligne = fichier.readline()
5|  print(ligne)
6|  fichier.close()
```

A la ligne 4 on lit une nouvelle ligne du fichier. Comme une ligne a déjà été lue, la première, ce sera donc la deuxième qui sera lue. Ainsi le premier `'print'` affiche la première ligne du fichier et le deuxième `'print'` affiche la deuxième ligne du fichier.

7\. Il faut faire une boucle.

```python
1|  ligne = None
2|  while ligne != '':
3|      ligne = fichier.readline()
4|      print(ligne)
5|  
6|  fichier.close()
```

La ligne 2 teste si la ligne lue est vide. Si c'est la cas la boucle s'arrête ainsi que la lecture du contenu du fichier et son affichage. En effet une ligne vide signifie qu'on est arrivé à la fin du fichier. Attention, avec cette méthode si il y a une ligne vide en plein milieu du fichier la boucle s'arrêtera. Normalement ça ne devrait pas être le cas car une ligne texte vide dans un fichier texte est toujours terminée par le caractère non imprimage `\n` ou les caractères non imprimables `\r\n` permettant le retour à la ligne.

A l'affichage on se rend compte qu'il y a des sauts de ligne entre chaque ligne du fichier. C'est normal car les `'print()'` imposent par défaut un retour à la ligne, mais chaque ligne possède elle-même leur retour à la ligne. Pour avoir un affichage fidèle au fichier, il faut supprimer le retour à la ligne par défaut du print en écrivant `print(ligne, end = '')`.

Il est possible d'utiliser une boucle `'for'` de la manière suivante.

```python
1|  fichier = open('mon_texte.txt', 'r', encoding='utf-8')
2|  
3|  for ligne in fichier:
4|      print(ligne)
5|  
6|  fichier.close()
```

Cette méthode permet aussi l'économie de la méthode `.readline()` qui est exécutée automatiquement.

8\. La méthode `.readlines()` permet effectivement de lire le fichier en une seule fois.

9\. L'avantage est que le fichier peut être fermé de suite. Ainsi sa lecture se fait en une fois ce qui permet d'éviter que des problèmes de lecture indépendants du programme Python viennent le perturber. Lorsqu'on le lit au fur et à mesure on augmente les chances de ce genre de problèmes.

L'inconvéniant est que tout le contenu du fichier est stocké en mémoire vive de l'ordinateur. Pour une fichier très volumineux, ce processus consomme beaucoup de mémoire et l'étape de lecture peut être très longue.

10\.  On obtient la liste des lignes contenues dans le fichier. Chaque ligne est terminée par le caractère non imprimable `'\n'` qui est le caractère $`\mathrm{0A|_{46}}`$, c'est à dire le onzième (caractère 10), de la table ASCII. C'est le Line Feed, LF.

11\. On ajoute les deux lignes ci-dessous au code précédent.

```python
1|  fichier = open('mon_texte.txt', 'r', encoding='utf-8')
2|  texte = fichier.readlines()
3|  fichier.close()
4|  
5|  print(texte)
6|  
7|  for ligne in texte:
8|      print(ligne)
```

12\. Pour supprimer le double retour à la ligne, on peut agir sur le `print()` comme à la question 7 mais on peut aussi supprimer le `'\n'` de chaque ligne lue. Pour cela on indexe chaque ligne avec `ligne[:-1]`, c'est-à-dire du caracère 0, sous entendu avant les `:`, à l'avant dernier caractère qui est le caractère `-1`.

13\. Le code donné permet d'obtenir les lignes du fichier sans les `'\n'` dans la variable `texte_liste`.

14\. On obtient le même résultat mais en générant la liste par compréhension.

15\. On teste le code. En ouvrant le fichier dans un éditeur de texte on s'aperçoit que la ligne `'Liste de courses du 8 Janvier 2020'` a été ajoutée.

16\. Pour avoir un retour à la ligne dans le fichier texte.

17\. On exécute deux fois de suite le code, ou une fois, avec deux `.write()`.

18\. On peut utiliser la méthode précédente ou la méthode avec le mot clé `with`. `utf-8` sera remplacé par `'iso-8859-1'` puis `'ascii'`.

```python
1|  with open('mon_texte.txt', 'r', encoding='utf-8') as fichier:
2|     texte = fichier.readlines()
3|  
4|  print(texte)
```

Avec `'iso-8859-1'`, les caractères commes les symboles monétaires ne sont pas affichés correctements. Avec `'ascii'` l'erreur `UnicodeDecodeError: 'ascii' codec can't decode byte` est soulevée car certains codes dépassent 127.

19\. L'option `errors = 'replace'` permet d'éviter qu'une erreur d'encodage n'arrête le programme. En cas d'erreur, le caractère concerné est remplacé, ici par `�`.
