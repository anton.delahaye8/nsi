# REPRESENTATION DES CARACTERES

# Correction

## I. Le type bytes – les octets en Python

1\. Le code convertit un octet en entier non signé.

```python
1  octet = b'\xe8'
2  entier = int.from_bytes(octet, byteorder='big', signed=False)
3  print("L'octet :", octet, "représente le nombre", entier)
```

* La ligne 1 définit l'octet à la valeur $`\mathrm{E8|_{16}}`$. C'est toujours un nombre binaire, ici $`\mathrm{1110 \ 1000‬|_{2}}`$. Il est juste spécifié en hexadécimal par convenance. `byteorder='big'` signifie que l'octet est écrit avec son bit de poids fort en premier (le 1 à gauche) et son bit de poids faible en dernier (le 0 à droite).
* La ligne 2 effectue la conversion. Bien noter le paramètre `signed=False` utilisé dans la fonction.
* La ligne 3 affiche le résultat de la conversion.

En effet $`\mathrm{E8|_{16}}`$, c'est à dire $`\mathrm{1110 \ 1000‬|_{2}}`$, correspond bien à $`\mathrm{232|_{10}}`$ en décimal. Valeur obtenue avec la décomposition en somme de puissance de deux.

2\. Le code convertit un octet en entier signé.

```python
1  octet = b'\xe8'
2  entier = int.from_bytes(octet, byteorder='big', signed=True)
3  print("L'octet :", octet, "représente le nombre", entier)
```

* La ligne 2 effectue la conversion. Bien noter le paramètre `signed=True` utilisé dans la fonction.

En effet $`\mathrm{E8|_{16}}`$, c'est à dire $`\mathrm{1110 \ 1000‬|_{2}}`$, correspond bien à $`\mathrm{-24|_{10}}`$ en décimal. Valeur obtenue avec la méthode du complément à deux.

## II. La norme ASCII

1\. Le code convertit un octet en caractère ASCII.

```python
1  octet = b'\x46'
2  code = int.from_bytes(octet, byteorder='big', signed=False)
3  caractere = octet.decode('ascii')
4  print("L'octet :", hex(code), "représente le caractère", caractere)
```

* La ligne 1 définit l'octet à la valeur $`\mathrm{46|_{16}}`$ c'est-à-dire  $`\mathrm{0100 \ 0110‬‬|_{2}}`$.
* La ligne 2 convertit l'octet en entier non signé. La valeur correspond au code ASCII du caractère.
* La ligne 3 convertit l'octet en caractère ASCII.
* La ligne 4 affiche le résultat.

En effet $`\mathrm{E8|_{46}}`$, c'est à dire $`\mathrm{0100 \ 0110‬|_{2}}`$, correspond au code ASCII $`\mathrm{70|_{10}}`$. Le caractère correspondant dans la table ASCII est bien un 'F'.

2\. `.decode()` convertit une suite d'octets, ici un seul, en chaine de caractères, ici un seul, selon l'encodage spécifié, ici ASCII.

3\. Il suffit de changer la valeur de l'octet dans le code de la question 1.

* $`\mathrm{2B|_{16}}`$ correspond à un '+'.
* $`\mathrm{55|_{16}}`$ correspond à un 'U'.
* $`\mathrm{08|_{16}}`$ est un caractère non imprimable correspondant à l'instruction machine [BACKSPACE].
* $`\mathrm{07|_{16}}`$ est un caractère non imprimable correspondant à l'instruction machine [BELL]. Sur certaines machines un son est émis (activer le haut parleur).

4\. Avec $`\mathrm{07|_{16}}`$ l'erreur `UnicodeDecodeError: 'ascii' codec can't decode byte 0xa7 in position 0: ordinal not in range(128)` est soulevée. Le caracère ASCII n'existe pas. En effet il correspond au code $`\mathrm{167|_{10}}`$. La norme n'en contient que 128, de $`\mathrm{0|_{10}}`$ à $`\mathrm{127|_{10}}`$.

5\. Soit le code ci-dessous.

```python
1  chaine = 'bonjour'
2  octets = chaine.encode('ascii')
3  print(octets.hex())
```

* La ligne 1 définit une chaîne de caractères.
* La ligne 2 convertit chaque caractère de la chaîne en octet selon la norme ASCII.
* La ligne 3 affiche la suite d'octets, sous forme hexadécimale. Sans la méthode `.hex()` la suite d'octets serait affichée par Python sous la forme de sa chaîne de caractères la plus proche. La seule différence avec une chaîne de caractères est le `b` juste devant les `''`. Cet affichage peut nuire à la compréhension.

6\. L'erreur `UnicodeEncodeError: 'ascii' codec can't encode character '\xe9' in position 10: ordinal not in range(128)` est soulevée. En effet la chaîne de caractères `bonjour Frédérique` contient des caractères accentués, non représentables en norme ASCII.

## III. La norme ISO 8859-1 ou LATIN-1

1\. La première partie de la table ISO 8859-1 correspond à la table ASCII, des caractères $`\mathrm{20|_{16}}`$ à $`\mathrm{7E|_{16}}`$. Par exemple le caractère $`\mathrm{41|_{16}}`$ est le 'A' dans les deux tables, le caractère $`\mathrm{28|_{16}}`$ est le '('.

2\. On écrit les lignes de code suivantes.

```python
1  octet = b'\xe8'
2  code = int.from_bytes(octet, byteorder='big', signed=False)
3  caractere = octet.decode('iso-8859-1')
4  print("L'octet", hex(code), "représente le caractère", caractere)
```

En effet l'entier non signé $`\mathrm{232|_{10}}`$ correspond à l'octet $`\mathrm{E8|_{16}}`$. Le caractère affiché est un 'è'.

3\. Il suffit d'écrire le programme précédent dans une boucle `for`.

```python
1  for i in range(160, 256):
2      octet = bytes([i])
3      code = int.from_bytes(octet, byteorder='big', signed=False)
4      caractere = octet.decode('iso-8859-1')
5      print("L'octet", hex(code), "représente le caractère", caractere)
```

4\. Le lien fournit donne les différentes tables ISO 8859.

https://fr.wikipedia.org/wiki/ISO/CEI_8859#Table_comparative_des_diverses_parties_d%E2%80%99ISO_8859

En les comparant, on se rend compte que le caractère $`\mathrm{A7|_{16}}`$, symbole paragraphe, est commun à toutes les tables sauf deux. Le caractère $`\mathrm{E8|_{16}}`$, caractère 'è', est présent dans 6 tables sur les 15. Elles sont numérotées de 1 à 16 mais la table 12 n'existe pas.


5\. Le symbole euro est codé dans les tables 5, 7, 15 et 16. Le code est $`\mathrm{A4|_{16}}`$.

6\. Le code permettant d'obtenir la représentation du caractère $`\mathrm{E8|_{16}}`$ de chaque table est le suivant.

```python
1  for i in range(16):
2      octet = b'\xe8'
3      table = 'iso-8859-' + str(i + 1)
4      code = int.from_bytes(octet, byteorder='big', signed=False)
5      if i + 1 != 12:
6          caractere = octet.decode(table)
7          print("L'octet", hex(code), "de la table", table, "représente le caractère", caractere)
```

* La ligne 1 permet de boucler 15 fois, pour `i` allant de 0 à 15.
* La ligne 3 permet d'obtenir le nom de la table associé à son numéro par concaténation de `i + 1`. Ainsi les numéros iront de 1 à 16.
* La ligne 5 permet de ne prendre en considération que les tables dont le numéro n'est pas 12.

7\. On a du faire attention au fait que la table ISO 8859-12 n'existe pas. Dans le cas contraire, l'erreur `LookupError: unknown encoding: iso-8859-12` est soulevée.

## IV La table UNICODE et sa représentation UTF-8

1\. Les trois lignes de code ci-dessous sont suffisantes :

```python
1  caractere = 'A'
2  octets = caractere.encode('utf-8')
3  print(octets.hex())
```

On teste pour 'A' puis pour '€'.

2\. On visite le lien donné.

https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(0000-0FFF)

En cliquant les différentes tables du plan 0 on se rend compte que la table 2000 – 2FFF contient un paragraphe "Symboles monétaires". On y trouve le symbole '€' avec comme code $`\mathrm{20AC|_{16}}`$, ce qui donne dans les autres bases $`\mathrm{8364|_{10}}`$ et $`\mathrm{0010 \ 0000 \ 1010 \ 1100‬|_{2}}`$.

3\. Non le code unicode ne correspond pas directement à sa représentation sous forme d'octets en mémoire. En effet le caractère '€' a comme code $`\mathrm{20AC|_{16}}`$ mais est représenté en mémoire par $`\mathrm{E28AC|_{16}}`$.

4\. $`\mathrm{28BA|_{16}}`$ est aussi dans la table 2000 – 2FFF. En la parcourant on trouve que c'est le motif Braille ⢺.

5\. En cherchant dans les tables, éventuellement le web, on trouve que le hiéroglyphe recherché dans la table 13000 – 13FFF du plan 1. 𓁿 est le symbole $`\mathrm{1307F|_{16}}`$. Ensuite on applique la procédure pour trouver sa représentation en mémoire.

* On converti le code unicode en binaire : $`\mathrm{1 \ 0011 \ 0000 \ 0111 \ 1111|_{2}}`$
* On le range par série de 6 bits, de droite à gauche : $`\mathrm{010011 \quad 000001 \quad 111111|_{2}}`$
* Il faudra donc 4 octets pour la représentation. Les trois derniers commenceront par $`\mathrm{\color{red}{10}}`$ : $`\mathrm{\color{red}{10} \color{black}{010011} \quad \color{red}{10} \color{black}{000001} \quad \color{red}{10} \color{black}{111111}|_{2}}`$
* Le premier commencera par $`\mathrm{\color{red}{11110}}`$ et il manquera trois $`\mathrm{\color{blue}{000}}`$ non significatifs pour compléter l'octet à 8 bits : $`\mathrm{\color{red}{11110} \color{blue}{000} \quad \color{red}{10} \color{black}{010011} \quad \color{red}{10} \color{black}{000001} \quad \color{red}{10} \color{black}{111111}|_{2}}`$
* Converti en hexadécimal : $`\mathrm{F09381BF|_{16}}`$

La vérification Python correspond au code ci-dessous.

```python
1  octet = b'\xf0\x93\x81\xbf'
2  code = int.from_bytes(octet, byteorder='big', signed=False)
3  caractere = octet.decode('utf-8')
4  print("L'octet", hex(code), "représente le caractère", caractere)
```

6\. On suit la procédure inverse.

* La représentation est écrite en binaire : $`\mathrm{11100011 \quad 10000110 \quad 10101110|_{2}}`$
* Le début du premier octet $`\mathrm{\color{red}{1110}}`$ indique que la représentation est composée de 3 octets ce qui est conforme. Les deux $`\mathrm{\color{blue}{00}}`$ suivants sont donc non significatifs : $`\mathrm{\color{red}{1110} \color{blue}{00} \color{black}{11} \quad 10000110 \quad 10101110|_{2}}`$
* Les deux autres octets commencent donc par $`\mathrm{\color{red}{10}}`$ : $`\mathrm{\color{red}{1110} \color{blue}{00} \color{black}{11} \quad \color{red}{10} \color{black}{000110} \quad \color{red}{10} \color{black}{101110}|_{2}}`$
* On identifie alors le code unicode écrit en binaire : $`\mathrm{11 \quad 000110 \quad 101110|_{2}}`$
* On le converti en hexadécimal : $`\mathrm{31AE|_{16}}`$

En cherchant dans les tables unicode, on identifie le caractère ㆮ.

La vérification Python correspond au code ci-dessous.

7\. `chr()` permet d'obtenir le caractère correspondant à un code unicode donné en décimal. Inversement ord() permet d'obtenir le code unicode décimal d'un caractère. Par exemple :

```python
1.  print(chr(77951))
2.  print(chr(33))
3.  print(ord('€'))
4.  print(ord('Z'))
```

8\. Les dex lignes ci-dessous montrent que l'encodage par défaut utilisé par Python est utf-8.

```python
1.  import sys
2.  print(sys.getdefaultencoding())
```
