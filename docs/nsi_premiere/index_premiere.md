# Ressources NSI - Classe de première

## Fonctionnement électrique de l'ordinateur - Circuits combinatoires

- Notions de cours :
  * Fiche décrivant les signaux électriques transmis en informatique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Signaux_numériques.pdf).
  * Fiche décrivant les principales fonctions logiques utilisées en électronique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Electronique_et_logique.pdf)
  * Article sur la problématique du Dark Silicon [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/Dark_Silicon.pdf)


- Activité sans ordinateur :
  * Sujet [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_TP_Representation_electrique_information.pdf)
  * Aide pour étudier le fonctionnement de l'additionneur [odp](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Aide_additionneur.odp)


- TP Python sur les circuits combinatoires : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques_python.pdf)
  * Ressources : [py](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques.py)


## Interaction client-serveur (protocoles TCP, HTTP, HTTPS)

La mise en pratique nécessite l'installation de [Wireshark](https://www.wireshark.org/). Il est disponible sous Linux, macOS et Windows. Sous ce dernier l'installation du driver [Npcap](https://nmap.org/npcap/) sera nécessaire pour acquérir les données échangées entre deux machines différentes.

L'activité peut très bien se faire en local sur la même machine. Elle sert alors à la fois de serveur et de client. Les applications serveur et client seront donc lancées en parallèle. Il faut pour cela utiliser les adresses de loopback. L'adresse IP client sera 127.0.0.1 et l'adresse IP serveur sera 127.0.0.X avec X entre 2 et 254, par exemple 127.0.0.20.

Enfin, l'activité pourrait être simulée sous [Filius](https://www.lernsoftware-filius.de/Herunterladen) à l'aide du mini-réseau construit dans le cours précédent.

- Notions de cours :
  * Fiche décrivant la mise en place d'un serveur Python [pdf](./06_ihm_web/03_client_serveur/04a_Fiche_eleve_Serveur_python.pdf)
  * Fiche décrivant la mise en place d'un serveur Python sécurisé [pdf](./06_ihm_web/03_client_serveur/04e_Fiche_eleve_Serveur_python_securise.pdf)
  * Fiche rappelant quelques notions sur les protocoles IP et TCP [pdf](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf)
  * Fiche rappelant quelques notions sur le protocole HTTP [pdf](./07_architecture_reseau/01_cours_reseau/04c_Activite_SNT_Fiche_eleve_HTTP.pdf)

- Mise en pratique :
  * Sujet [pdf](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.pdf)
  * Fichiers nécessaires [zip](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.zip)


## Représentation des caractères

- Notions de cours et mise en pratique : [pdf](./05_representation_donnees/05_caracteres/representation_des_caracteres.pdf)
- Illustrations : [pdf](./05_representation_donnees/05_caracteres/illustrations_cours.pdf)
- exercice HTML et charset : [pdf](./05_representation_donnees/05_caracteres/HTML_charset.pdf)


## Protocole du bit alterné

- Notions de cours : [pdf](./07_architecture_reseau/03_bit_alterne/bit_alterne.pdf)


## Les tuples

- cours et activités : [pdf](./04_python_sequences/Cours_Python_04c_Les_sequences_tuples.pdf)


## Les listes

- cours et activités : [pdf](./04_python_sequences/Cours_Python_04d_Les_sequences_listes.pdf)


## Les dictionnaires

Les dictionnaires sont une structure de donnée mutable qui permet d'identifier chacun de ses éléments grâce à une clé, qui peut être une chaîne de caractère :

- cours et activités : [pdf](./04_python_sequences/Cours_Python_04e_Les_dictionnaires.pdf)


## Traitement des données en table

- Notions et activités autour des fichiers textes et de leur encodage : [pdf](./05_representation_donnees/05_caracteres/ouverture_fichiers_texte.pdf)

- Cours sur les données et leur structuration :  [pdf](./09_traitement_donnees/cours_donnees_structuration.pdf)

Introduction au traitement de données en utilisant les tableaux (listes) à deux dimensions pour représenter les tables.

- Notions et activités sur les fichiers CSV : [pdf](./09_traitement_donnees/ouverture_fichiers_csv.pdf)

- Activités sur le traitement des données : [pdf](./09_traitement_donnees/traitement_donnees_table.pdf)

  * Ressources : [zip](./09_traitement_donnees/pokemon_stats.zip)


## Algorithmique - Complexité

- Introduction à l'algorithmique : [pdf](./10_algorithmique/cours_algo_1_intro.pdf)

- Complexité en temps : [pdf](./10_algorithmique/cours_algo_2_complexite.pdf)

- Correction d'un algorithme : [pdf](./10_algorithmique/cours_algo_3_correction.pdf)


## Parcours séquentiel d'un tableau

- Travail pratique autour du calcul de moyenne, la recherche d'extrémum et d'occurences : [md](./10_algorithmique/TP_algo_01_parcours_tableau.md)


## Les tris

- Tri par sélection [pdf](./10_algorithmique/02_tris/SR_01_tri_selection.pdf) :
à chaque étape on sélectionne tout simplement le plus petit élément restant que l'on place à la suite dans la liste triée

- Tri par insertion [pdf](./10_algorithmique/02_tris/SR_02_tri_insertion.pdf) :
chaque élément est tour à tour inséré à sa place dans la liste triée

- Travail pratique autour des tris : [md](./10_algorithmique/TP_algo_02_tris.md)


## Recherche dichotomique

- Travail pratique autour de la recherche dichotomique : [md](./10_algorithmique/TP_algo_03_recherche_dichotomique.md)


## Intelligence artificielle ?

- Travail pratique autour de l'algorithme des plus proches voisins et les algorithmes gloutons : [md](./10_algorithmique/TP_algo_04_algotithmes_intelligents.md)


## Mini projet guidé : la machine à inventer des mots.

- Video introductive : [Science étonnante](https://sciencetonnante.wordpress.com/2015/11/06/la-machine-a-inventer-des-mots-version-ikea/)

- Guide Python : [pdf](./00_projets/machine_mots/machine_inventer_mots.pdf)

  - Ressources : [zip](./00_projets/machine_mots/ressources_mots.zip)


## Quelques corrections

- Fonctionnement de l'additionneur complétée : [odp](./08_architecture_ordinateur/02_circuits_combinatoires/correction_SR_05_Aide_additionneur.odp)

- TP Python sur les circuits combinatoires : [md](./08_architecture_ordinateur/02_circuits_combinatoires/correction_fonctions_logiques_python.md)

- Cours sur la représentation des caractères : [md](./05_representation_donnees/05_caracteres/correction_representation_des_caracteres.md)

- Exercice HTML et charset : [md](./05_representation_donnees/05_caracteres/correction_HTML_charset.md)

- Cours sur les tableaux (listes) : [md](./04_python_sequences/correction_Cours_Python_04d_Les_sequences_listes.md)

- Cours sur les dictionnaires : [md](./04_python_sequences/correction_Cours_Python_04e_Les_dictionnaires.md)

- Les fichiers textes et leur encodage : [md](./05_representation_donnees/05_caracteres/correction_ouverture_fichiers_texte.md)

- Les fichiers CSV : [md](./09_traitement_donnees/correction_ouverture_fichiers_csv.md)

- Traitement des données : [md](./09_traitement_donnees/correction_traitement_donnees_table.md)

- Travail pratique autour du calcul de moyenne, la recherche d'extrémum et d'occurences : [md](./10_algorithmique/correction_TP_algo_01_parcours_tableau.md)

- Travail pratique autour des tris : [md](./10_algorithmique/correction_TP_algo_02_tris.md)
