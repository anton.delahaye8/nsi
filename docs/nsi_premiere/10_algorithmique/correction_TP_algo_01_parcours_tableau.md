# Parcours séquentiel d’un tableau

# Correction

## I. Calcul d'une moyenne

1\. On code le programme suivant.

```python
1|  import random
2|  
3|  somme = 0
4|  
5|  for i in range(len(liste_nombres)):
6|     somme = somme + liste_nombres[i]
7|  
8|  moyenne = somme / len(liste_nombres)
```

Pour le tester sur 30 nombres aléatoires on ajoute les lignes suivantes juste avant la ligne 3.

```python
n = 30
liste_nombres = [random.randint(0, 100) for i in range(n)]
```

2\. On dénombre les opérations.

*  Ligne 6 : une addition et une affectation, donc $`\mathrm{T(n) = 2}`$ opérations.
*  Ligne 5 : une affectation supplémentaire pour le `i`, donc $`\mathrm{T(n) = 3}`$ opérations.
*  Ligne 5 : la boucle `for` va répéter $`\mathrm{n}`$ fois les $`\mathrm{3}`$ opérations, donc $`\mathrm{T(n) = 3.n}`$ opérations.
* Ligne 3 et 8 : on ajoute deux affectations et une division à l'ensemble, donc $`\mathrm{T(n) = 3.n + 3}`$ opérations.


3\. Une boucle `for` se termine toujours car son nombre d'itérations est fixé au départ. Donc l'algorithme se termine.

4\. On choisi comme propriété la relation : $`\mathrm{somme(n) = \displaystyle\sum_{i=0}^n liste\_nombres[i]}`$.

1. Avant l'exécution de la boucle, `somme` vaut 0 dans l’algorithme. Effectivement la boucle n’est pas exécutée car `range(0)` retourne une liste vide. C’est aussi le cas de la propriété puisque le calcul de la somme n'est pas commencé.

2. On suppose que la propriété est vérifiée après l’itération n.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ainsi, $`\mathrm{somme(n) = \displaystyle\sum_{i=0}^n liste\_nombres[i]}`$.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Après l’itération $`\mathrm{n + 1}`$, l’algorithme donne : $`\mathrm{somme(n + 1) = somme(n) +  liste\_nombres[n + 1]} = \displaystyle\sum_{i=0}^n liste\_nombres[i] + liste\_nombres[n + 1] = \displaystyle\sum_{i=0}^{n+1} liste\_nombres[i]`$

3. Comme la propriété est vérifiée par l’algorithme à n’importe quelle itération n + 1 si elle l’est à n’importe quelle itération précédente n, et qu’elle est vérifiée dès le départ, la propriété est donc vérifiée à toutes les itérations. L’algorithme calcule donc entre autre la propriété, ce qui est le résultat attendu, puisque la moyenne correspond à la propriété divisée par la taille des données qui est un nombre constant.

5\. Ecrire le programme suivant en insérant votre algorithme à la ligne 8.

 ```python
  1|  import time
  2|  import random
  3|  
  4|  n = 30
  5|  liste_nombres = [random.randint(0, 100) for i in range(n)]
  6|  
  7|  t_debut = time.perf_counter()
  8|  
  9|  somme = 0
10|  
11|  for i in range(len(liste_nombres)):
12|     somme = somme + liste_nombres[i]
13|  
14|  moyenne = somme / len(liste_nombres)
15|  
16|  t_fin = time.perf_counter()
17|  
18|  print('''temps d'exécution :''', t_fin - t_debut)
```

6 et 7\. On obtient la courbe ci-dessous (Roussel Thaïs).

![](assets/correction_TP_algo_01_parcours_tableau-58f37550.png)

C'est le résultat attendu car les points sont alignés selon une droite et $`\mathrm{T(n) = 3.n + 3}`$ est une fonction affine.

8\. Si on modélisait la courbe par son équation mathématique, on ne pourrait pas tenir compte des valeurs obtenues pour les coefficients car ils dépendent notamment de la machine, de l'environnement logiciel et du langage de programmation utilisé.


## II. Recherche d'un extrémum

1\. On code le programme suivant.

```python
1|  import random
2|  n = 30
3|  liste_nombres = [random.randint(0, 100) for i in range(n)]
4|  
5|  valeur_max = liste_nombres[0]
6|  
7|  for i in range(1, len(liste_nombres)):
8|     if liste_nombres[i] > valeur_max:
9|         valeur_max = liste_nombres[i]
```

2\. On dénombre les opérations.

*  Ligne 8 et 9 : une comparaison suivi d'une affectation, donc $`\mathrm{T(n) = 2}`$ opérations.
*  Ligne 7 : une affectation supplémentaire pour le `i`, donc $`\mathrm{T(n) = 3}`$ opérations.
*  Ligne 7 : la boucle `for` va répéter $`\mathrm{n - 1}`$ fois les $`\mathrm{3}`$ opérations, donc $`\mathrm{T(n) = 3.(n - 1)}`$ opérations.
* Ligne 5 : on ajoute deux affectations et une division à l'ensemble, donc $`\mathrm{T(n) = 3.(n - 1) + 3 = 3.n + 2}`$ opérations.

Les lignes 2 et 3 ne font pas partie de l'algorithme de recherche. Ce sont les données qu'il utilise.

3\. Une boucle `for` se termine toujours car son nombre d'itérations est fixé au départ. Donc l'algorithme se termine.

4 et 5\. Le pire des cas correspond à la recherche d'un maximum en commençant au début d'une liste triée. Il suffit donc de trier la liste dans l'ordre croissant juste après sa création. On ajoute aussi les lignes de codes permettant de mesurer le temps d'exécution. Ainsi :

```python
 1|  n = 20
 2|  liste_nombres = [random.randint(0, 100) for i in range(n)]
 3|  liste_nombres.sort()
 4|  
 5|  t_debut = time.perf_counter()
 6|  
 7|  valeur_max = liste_nombres[0]
 8|  
 9|  for i in range(1, len(liste_nombres)):
10|      if liste_nombres[i] > valeur_max:
11|          valeur_max = liste_nombres[i]
12|  
13|  t_fin = time.perf_counter()
14|  
15|  print('''temps d'exécution :''', t_fin - t_debut)
```

Les mesures sont représentées ci-dessous (Roussel Thaïs).

![](assets/correction_TP_algo_01_parcours_tableau-ed7b9b55.png)

Les points sont alignés selon une droite. C'est le résultat attendu car  $`\mathrm{T(n) = 3.n + 2}`$ est une fonction affine.


## III. Recherche d'une occurence

1\.  On code le programme suivant.

```python
 1|  n = 30
 2|  liste_nombres = [random.randint(0, 100) for i in range(n)]
 3|  element_recherche = 40
 4|  
 5|  occurence_trouvee = False
 6|  i = 0
 7|  
 8|  while occurence_trouvee != True and i <= len(liste_nombres) - 1:
 9|      if liste_nombres[i] == element_recherche:
10|          occurence_trouvee = True
11|      else:
12|          i = i + 1
```

2\. Dans le pire des cas l'algorithme effectue toutes les itérations et la partie du test la plus coûteuse, c'est à dire ici le `else`.

On dénombre les opérations.

* Ligne 9 et 11 : une comparaison suivie d'une addition et d'une affectation, donc $`\mathrm{T(n) = 3}`$ opérations. La ligne 10 correspond à un cas moins pire (une affectation).
* Ligne 8 : à cela il faut ajouter deux comparaisons et une opération `and`, donc $`\mathrm{T(n) = 6}`$ opérations.
* Ligne 8 : la boucle `while` va répéter $`\mathrm{n}`$ fois les $`\mathrm{3}`$ opérations, donc $`\mathrm{T(n) = 6.n}`$ opérations.
* Ligne 5 et 6 : on ajoute deux affectations à l'ensemble, donc $`\mathrm{T(n) = 6.n + 2}`$ opérations.

Les lignes 1, 2 et 3 ne font pas partie de l'algorithme de recherche. Ce sont les données qu'il utilise.

3\. Une boucle `for` se termine toujours car son nombre d'itérations est fixé au départ. Donc l'algorithme se termine.

4 et 5\. Le pire des cas correspond à la recherche d'un nombre qui n'existe pas dans la liste, soit par exemple un nombre négatif. On ajoute aussi les lignes de codes permettant de mesurer le temps d'exécution. Ainsi :

```python
 1|  n = 200
 2|  liste_nombres = [random.randint(0, 100) for i in range(n)]
 3|  element_recherche = -40
 4|  
 5|  t_debut = time.perf_counter()
 6|  
 7|  occurence_trouvee = False
 8|  i = 0
 9|  
10|  while occurence_trouvee != True and i <= len(liste_nombres) - 1:
11|      if liste_nombres[i] == element_recherche:
12|          occurence_trouvee = True
13|      else:
14|          i = i + 1
15|  
16|  t_fin = time.perf_counter()
17|  
18|  print(occurence_trouvee, i)
19|  
20|  print('''temps d'exécution :''', t_fin - t_debut)
```

Les mesures sont représentées ci-dessous (Roussel Thaïs).

![](assets/correction_TP_algo_01_parcours_tableau-3a17e06f.png)

Les points sont alignés selon une droite. C'est le résultat attendu car  $`\mathrm{T(n) = 3.n + 2}`$ est une fonction affine.
