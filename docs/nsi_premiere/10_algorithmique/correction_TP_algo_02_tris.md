# Les tris

# Correction

## I. Tri par sélection

1\. Le programme avec mesure du temps d'exécution est le suivant.

```python
 1|  n = 30
 2|  liste_nombres = [random.randint(0, 100) for i in range(n)]
 3|  
 4|  print(liste_nombres)
 5|  
 6|  t_debut = time.perf_counter()
 7|  
 8|  for i in range(len(liste_nombres)):
 9|      plus_petit_non_en_place = i
10|      for j in range(i, len(liste_nombres)):
11|          if liste_nombres[j] < liste_nombres[plus_petit_non_en_place]:
12|              plus_petit_non_en_place = j
13|      temp = liste_nombres[i]
14|      liste_nombres[i] = liste_nombres[plus_petit_non_en_place]
15|      liste_nombres[plus_petit_non_en_place] = temp
16|  
17|  t_fin = time.perf_counter()
18|  
19|  print('''temps d'exécution :''', t_fin - t_debut)
20|  
21|  print(liste_nombres)
```

2\. On dénombre les opérations.

*  Ligne 12 : une affectation, donc $`\mathrm{T(n) = 1}`$ opération.
*  Ligne 11 : une comparaison, donc $`\mathrm{T(n) = 2}`$ opérations.
*  Ligne 10 : affectation du `j`, donc $`\mathrm{T(n) = 3}`$ opérations.
*  Ligne 10 : boucle exécutée `n` fois dans le pire des cas, donc $`\mathrm{T(n) = 3.n}`$ opérations.
*  Ligne 10 : initialisation du `range()`, donc $`\mathrm{T(n) = 3.n + 1}`$ opérations.
*  Lignes 9, 13, 14 et 15  : quatre affectations, donc $`\mathrm{T(n) = 3.n + 5}`$ opérations.
*  Ligne 8 : affectation du `i`, donc $`\mathrm{T(n) = 3.n + 6}`$ opérations.
*  Ligne 8 : boucle exécutée `n` fois, donc $`\mathrm{T(n) = (3.n + 6).n}`$ opérations.
*  Ligne 8 : initialisation du `range()`, donc $`\mathrm{T(n) = (3.n + 6).n + 1}`$ opérations.

Au final $`\mathrm{T(n) = (3.n + 6).n + 1 = 3.n^2 + 6.n + 1}`$ opérations.

3\. L'algorithme est constitué de deux boucles `for` qui se terminent toujours. Donc l'algorithme se termine.

4\. Deux propriétés doivent ici être envisagées comme invariants.

* propriété 1 : à l'étape `i`, `liste_nombres[i]` est le plus petit élément de `liste_nombres[i]` à `liste_nombres[n - 1]`.
* propriété 2 : à l'étape `i`, `liste_nombres[0]` à `liste_nombres[i]` est triée.

1. A l'étape `i = 0` si `liste_nombres[0]` n'est pas le plus petit élément de `liste_nombres[i]` à `liste_nombres[n - 1]`, il le devient grâce à la boucle `for j in range(0, len(liste_nombres))`. La propriété 1 est vérifiée.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`liste_nombres[0]` à `liste_nombres[0]` ne contient qu'un seul élément. Cette partie de la liste est donc triée. La propriété 2 est vérifiée.

2. On suppose les deux propriétés vérifiées à l'étape `i`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A l'étape `i + 1` si `liste_nombres[i + 1]` n'est pas le plus petit élément de `liste_nombres[i]` à `liste_nombres[n - 1]`, il le devient grâce à la boucle `for j in range(i + 1, len(liste_nombres))`. La propriété 1 devient vraie à l'étape `i + 1`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comme $`\mathrm{liste\_nombres[i + 1] \ge liste\_nombres[i]}`$, puisque `liste_nombres[i]` est le plus petit élément de `liste_nombres[i]` à `liste_nombres[n - 1]`, et `liste_nombres[0]` à `liste_nombres[i]` est triée, alors `liste_nombres[0]` à `liste_nombres[i + 1]` est triée. La propriété 2 devient vraie à l'étape `i + 1`.

3. Comme les deux propriétés sont vérifiées par l’algorithme à n’importe quelle itération `i + 1` si elles le sont à n’importe quelle itération précédente `i`, et qu’elles sont vérifiées dès le départ, les deux propriétés sont donc vérifiées à toutes les itérations. L’algorithme réalise donc les propriétés, ce qui est le résultat attendu, puisque la liste sera triée.

5 et 6\. On obtient la courbe ci-dessous (Roussel Thaïs).

![](assets/correction_TP_algo_02_tris-1e14bd56.png)

On obtient une parabole. C'est le résultat attendu puisque $`\mathrm{T(n)}`$ est une fonction du second degré.


## II. Tri par insertion

1\. Le programme avec mesure du temps d'exécution est le suivant.

```python
 1|  n = 30
 2|  liste_nombres = [random.randint(0, 100) for i in range(n)]
 3|  
 4|  print(liste_nombres)
 5|  
 6|  t_debut = time.perf_counter()
 7|  
 8|  for i in range(1, len(liste_nombres)):
 9|      temp = liste_nombres[i]
10|      j = i
11|      while j > 0 and liste_nombres[j - 1] >= temp:
12|          liste_nombres[j] = liste_nombres[j - 1]
13|          j = j - 1
14|      liste_nombres[j] = temp
15|  
16|  t_fin = time.perf_counter()
17|  
18|  print('''temps d'exécution :''', t_fin - t_debut)
19|  
20|  print(liste_nombres)
```

2\. On dénombre les opérations.

*  Ligne 12 : une affectation, donc $`\mathrm{T(n) = 1}`$ opération.
*  Ligne 13 : une soustraction et une affectation, donc $`\mathrm{T(n) = 3}`$ opérations.
*  Ligne 11 : deux comparaisons et une opération logique, donc $`\mathrm{T(n) = 6}`$ opérations.
* Ligne 11 : une boucle `while` qui sera exécutée au maximum `n` fois, donc $`\mathrm{T(n) = 6.n}`$ opérations.
*  Ligne 9, 10 et 14 : trois affectations, donc $`\mathrm{T(n) = 6.n + 3}`$ opérations.
*  Ligne 8 : affectation du `i`, donc $`\mathrm{T(n) = 6.n + 4}`$ opérations.
*  Ligne 8 : boucle exécutée `n - 1` fois, donc $`\mathrm{T(n) = (6.n + 4).(n-1)}`$ opérations.
*  Ligne 8 : initialisation du `range()`, donc $`\mathrm{T(n) = (6.n + 4).(n - 1) + 1}`$ opérations.

Au final $`\mathrm{T(n) = (6.n + 4).(n - 1) + 1 = 6.n^2 + 4.n + 1 - 6.n - 4 = 6.n^2 - 2.n - 3}`$ opérations.

3\. La boucle `for` se termine toujours, donc il faut juste démontrer la terminaison de la boucle `while`.

La sortie de boucle est conditionnée par deux comparaisons liées par une opération logique `and`. Donc si une des deux comparaisons devient nécessairement fausse, la boucle s'arrête. Etudions la première comparaison qui semble être la plus facile à traiter.

La variable `j` joue le rôle d'un compteur lors de l'exécution de la boucle.

1. `j` est un nombre strictement positif avant l'exécution de la boucle, puisqu'il est initialisé à la valeur de `i`, qui est lui même fixé entre `1` et `n-1` grâce au `for i in range(1, len(liste_nombres))`.
2. `j` décroît strictement à chaque itération puisque à chaque étape on a l’affectation `j = j - 1`.
3. lorsque `j` est inférieur ou égal à `0` la condition `j > 0` devient fausse et la boucle s’arrête.

La boucle `while` se termine donc. Il est inutile d'étudier la deuxième comparaison.

Comme la boucle `for` se termine aussi, on en conclut que l'algorithme se termine.

4\. Deux propriétés doivent ici être envisagées comme invariants.

* propriété 1 : à l'étape `i`, `liste_nombres[i]` est à sa place ou a une place entre `liste_nombres[0]` et `liste_nombres[i - 1]`.
* propriété 2 : à l'étape `i`, `liste_nombres[0]` à `liste_nombres[i]` est triée.

1. A l'étape `i = 0`, la boucle n'a pas encore commencé, `liste_nombres[0]` est à sa place. La propriété 1 est vérifiée.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`liste_nombres[0]` à `liste_nombres[0]` ne contient qu'un seul élément. Cette partie de la liste est donc triée. La propriété 2 est vérifiée.

2. On suppose les deux propriétés vérifiées à l'étape `i`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A l'étape `i + 1` si `liste_nombres[i + 1]` n'est pas à sa place alors sa place est trouvée entre `liste_nombres[0]` et `liste_nombres[i]`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comme `liste_nombres[0]` à `liste_nombres[i]` est triée et que `liste_nombres[i + 1]` est inséré à sa place en conservant l'ordre, alors `liste_nombres[0]` à `liste_nombres[i + 1]` est triée. La propriété 2 devient vraie à l'étape `i + 1`.

3. Comme les deux propriétés sont vérifiées par l’algorithme à n’importe quelle itération `i + 1` si elles le sont à n’importe quelle itération précédente `i`, et qu’elles sont vérifiées dès le départ, les deux propriété sont donc vérifiées à toutes les itérations. L’algorithme réalise donc les propriétés, ce qui est le résultat attendu, puisque la liste sera triée.

5 et 6\. On obtient la courbe ci-dessous (Roussel Thaïs).

![](assets/correction_TP_algo_02_tris-6ed251e3.png)

On obtient une parabole. C'est le résultat attendu puisque $`\mathrm{T(n)}`$ est une fonction du second degré.
