# Les dictionnaires

# Correction

1\. Soit le code ci-dessous.

```python
1|  inventaire_fruits = {}
2|  
3|  inventaire_fruits["pommes"] = 5
4|  inventaire_fruits["poires"] = 3
5|  
6|  print(inventaire_fruits)
```

* Ligne 1 : définition d'un dictionnaire vide nommé `inventaire_fruits`.
* Ligne 3 : ajout de la clé `pomme` associée à la valeur `5`.
* Ligne 4 : ajout de la clé `poire` associée à la valeur `3`.
* Ligne 6 : affichage du dictionnaire dans la console.

2\.Pour ajouter à l’inventaire 10 kg de tomates, 2.5 kg de cerises et 16 kg d’oranges on écrit les lignes de code :

```python
1|  inventaire_fruits["tomates"] = 10
2|  inventaire_fruits["cerises"] = 2.5
3|  inventaire_fruits["oranges"] = 16
```

3\. Pour créer le dictionnaire en une seule ligne de code on écrit :

```python
1|  inventaire_fruits = {'pommes': 5, 'poires': 3, 'tomates': 10, 'cerises': 2.5, 'oranges': 16}
```

4\. On écrit et on teste les lignes de codes ci-dessous à la suite des précédentes.

```python
 1|  print(inventaire_fruits["tomates"])   	
 2|  
 3|  inventaire_fruits["poires"] = 1   	
 4|  print(inventaire_fruits)   	
 5|  
 6|  inventaire_fruits["pommes"] = inventaire_fruits["pommes"] + 4.7   	
 7|  print(inventaire_fruits)   	
 8|  
 9|  print(inventaire_fruits["abricots"])   	
10|  
11|  print(len(inventaire_fruits))
```

* Ligne 1 : on extrait la valeur de la clé `"tomates"`.
* Ligne 3 : on modifie la valeur de la clé `"poires"`.
* Ligne 6 : on incrémente la valeur de la clé `"pommes"`.
* Ligne 9 : l'erreur `KeyError` est soulevée car la clé `"abricots"` n'existe pas.
* Ligne 11 : on affiche la taille du dictionnaire.

5\. Si la clé que l'on veut extraire n'existe pas, `.get()` ne soulève pas d'erreur mais retourne la valeur `None`. Cette méthode permettra de travailler avec des clés qui n'existent pas forcément sans bloquer le programme.

6\. `"kiwis" in inventaire_fruits` vaut `True` si la clé `"kiwis"` est présente dans le dictionnaire, `False` dans le cas contraire.

7\. Le mot clé `del` permet de supprimer des clés dans un dictionnaire.

8\. La méthode `.keys()` retourne la liste des clés et la méthode `.values()` retourne la liste des valeurs.

9\. La méthode `.items()` retourne les tuples (clé, valeur) du dictionnaire.

10\. A l'affichage on voit bien que Python ne retourne pas les données brutes mais des objets `dict_keys`, `dict_values` et `dict_items`. Si nécessaire, pour les transformer en liste il faut utiliser la fonction `list()`. Pour les transformer en tuples on utiliserait la fonction `tuple()`.

11\. La fonction `dict()` fait l'opération inverse sur une séquence de couples (clé, valeur).

12\. `for el in inventaire_fruits.keys()` balaie les clés du dictionnaire. De même en remplaçant par `.values()` ou `.item()` on balaie les valeurs ou les couples (clé, valeur).

13\. `for cle, valeur in inventaire_fruits.items()` permet de balayer les couples (clé, valeur) tout en stockant à chaque itération (étape), chaque clé dans la variable 'cle' et chaque valeur dans la variable `valeur`.

14\. Le dictionnaire `inventaire_fruits_2` est un alias du dictionnaire `inventaire_fruits`. Ces deux références modifient en fait la même donnée stockée une seule fois et au même endroit de la mémoire.

15\. Comme pour les autres structures de données, pour obtenir deux dictionnaires différents en mémoire il faut utiliser la méthode `.copy`.

16\. `inventaire_fruits.update(ajouts)` permet de concaténer le dictionnaire `ajouts` au dictionnaire `inventaire_fruits`. C'est celui-ci qui est modifié.

17\. Soit le code ci-dessous.

```python
1|  inventaire_fruits = {"pommes": 5, "poires": 17, "tomates": 10, "cerises": 2.5, "oranges": 8}
2|  cles = list(inventaire_fruits.keys())
3|  cles.sort()
4|  print('''Inventaire du jour :''')
5|  for el in cles:
6|      print('  ', el, ' :', inventaire_fruits[el], 'kg')
```

* Ligne 1 : définition du dictionnaire `inventaire_fruits`.
* Ligne 2 : extraction de ses clés sous forme de liste.
* Ligne 3 : utilisation de la méthode `.sort()` pour trier par ordre croissant la liste (paramètre par défaut). Ici ce sera l'ordre alphabétique car les clés sont des chaînes de caractères.
* Ligne 5 : balayage de la liste des clés triée.
* Ligne 6 : extraction et affichage des valeurs du dictionnaires pour chaque clé, dans l'ordre alphabétique

18\. En cherchant dans la documentation de Python ou d'autres sites web, on trouve les différents paramètres de la méthode `.sort()`. `cles.sort(reverse = True)` permet de trier dans l'ordre décroissant.

19\. Le code ci-dessous recherche un des fruits présent en la plus grande quantité.

```python
1|  cles = list(inventaire_fruits.keys())
2|  valeurs = list(inventaire_fruits.values())
3|  valeur_max = max(valeurs)
4|  indice_max = valeurs.index(valeur_max)
5|  fruit_quantite_max = cles[indice_max]
6|  print('Les', fruit_quantite_max, 'sont en plus grande quantité avec', valeur_max, 'kg.')
```

En remplaçant `max()` par `min()` on obtient :

```python
1|  cles = list(inventaire_fruits.keys())
2|  valeurs = list(inventaire_fruits.values())
3|  valeur_min = min(valeurs)
4|  indice_min = valeurs.index(valeur_min)
5|  fruit_quantite_min = cles[indice_min]
6|  print('Les', fruit_quantite_min, 'sont en plus petite quantité avec', valeur_min, 'kg.')
```
