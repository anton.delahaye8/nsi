# Les séquences 3/3 - Tableaux

# Correction

18\. On accède aux élément du tableau 3D grâce à l'indexation. Il faut mettre 3 indices dans des crochets `[]` séparés.

```python
1|  tab = [[[5, 2, 0], [8, 7, 4], [7, 3, 1], [9, 7, 4]], [[0, 2, 8], [3, 8, 4], [2, 3, 0], [8, 3, 3]], [[8, 4, 0], [1, 2,
5],  [1, 7, 9], [7, 3, 3]], [[4, 1, 0], [9, 5, 0], [8, 2, 5], [3, 9, 7]], [[9, 1, 9], [2, 5, 7], [0, 1, 6], [3, 1, 4]]]

print(tab[2])
print(tab[2][1])
print(tab[2][1][1])
```

19\. Explications du code ci-dessous.

```python
1|  import random
2|  a = []
3|  for i in range(5):
4|      a.append([])
5|      for j in range(4):
6|          a[i].append([])
7|          for k in range(3):
8|              a[i][j].append(random.randint(0, 9))
```

* Ligne 1 : importation du module `random` permettant d'utiliser des nombres aléatoires.
* Ligne 2 : définition de la variable `a` contenant un tableau vide.
* Ligne 3 : boucle `for` qui sera répétée 5 fois. L'entier `i` prendra les valeurs de 0 à 4.
* Ligne 4 : ajout d'un tableau vide dans `a`. Ceci permet d'ajouter la première dimension au tableau.
* Ligne 5 : boucle `for` qui sera répétée 4 fois. L'entier `j` prendra les valeurs de 0 à 3.
* Ligne 6 : ajout d'un tableau vide dans `a[i]`. Ceci permet d'ajouter la deuxième dimension au tableau.
* Ligne 7 : boucle `for` qui sera répétée 3 fois. L'entier `k` prendra les valeurs de 0 à 2.
* Ligne 8 : ajout d'une valeur aléatoire dans `a[i][j]`. Ceci permet d'ajouter la troisième dimension au tableau.

20\. `a[0]`, `a[1]`, `a[2]`, ..., sont des tableaux 2D. Ainsi `a` est bien une liste de tableaux 2D puisque c'est la liste `[a[0], a[1], a[2], ...]`.

21\. Oui on peut créer un tableau à 7 dimensions. Ce sera une liste de tableaux à 6 dimensions, qui seront eux-mêmes des listes de tableaux à 5 dimensions, qui seront eux-mêmes des listes de tableaux à 4 dimensions, qui seront eux-mêmes des listes de tableaux à 3 dimensions, qui seront eux-mêmes des listes de tableaux à 2 dimensions, qui seront eux-mêmes des listes de tableaux à 1 dimension, qui seront des listes de valeurs.

22\. La construction de tableaux par compréhension est une notion importante à bien comprendre et utiliser. Les explications pour chaque exemple sont données ci-dessous.

* `i for i in range(7)` produit le nombre `i` pour i allant de 0 à 6. Entre crochets `[]` on produit ainsi une liste de 7 nombres entiers successifs.
* `1 for i in range(7)` produit le nombre `1`, 7 fois, pour i allant de 0 à 6.
* `'yo' for i in range(7)` produit la chaîne de caractères `'yo'`, 7 fois, pour i allant de 0 à 6.
* de même le nombre aléatoire `random.randint(0, 9)` est produit 7 fois.
* `el ** 2 for el in liste if el <= 10` produit les carrés des éléments de la liste `liste` si les éléments sont inférieurs ou égales à 10.

23\. Le code ci-dessous produit la liste des racines carrées de nombres positifs ou nuls de la liste.

```python
1|  import math
2|  
3|  liste = [1, -5, 7, 15, -2, 2, 56, 9, 0, -55, -9, 5, 12]
4|  e = [math.sqrt(el) for el in liste if el >= 0]
5|  print(e)
```

Si on enlève le `if` l'erreur `math domain error` est soulevée.

24\. Bien faire attention aux alias comme expliqué dans le TP. Ils sont implicitement très présents en Python et source de bon nombres d'erreurs.

25\. On peut tout simplement reprendre l'exemple précédent avec la méthode `.copy()`.

```python
1|  a = [1, 2, 3]
2|  b = a.copy()
3|  print(a, b)
4|  a[0] = 'aie'
5|  print(a, b)
6|  b[2] = 'ouille'
7|  print(a, b)
```

Ainsi les listes `a` et `b` sont bien différentes. Modifier `a` à la ligne 4 ne modifie par `b`. De même modifiant `b` à la ligne 6 ne modifie pas `a`.
