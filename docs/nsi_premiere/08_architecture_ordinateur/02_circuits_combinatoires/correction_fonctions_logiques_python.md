# Circuits combinatoires - Python

# Correction

## I. Fonctions logiques

2\. Dans la partie "Programme principal" on écrit :

``` python linenums="1"
print('Table de vérité de la fonction NAND :')
print('nand(0, 0) =', int(nand(0, 0)))
print('nand(0, 1) =', int(nand(0, 1)))
print('nand(1, 0) =', int(nand(1, 0)))
print('nand(1, 1) =', int(nand(1, 1)))

print()

print('Table de vérité de la fonction 1 :')
print('fonction1(0) =', int(fonction1(0)))
print('fonction1(1) =', int(fonction1(1)))

print()

print('Table de vérité de la fonction 2 :')
print('fonction2(0, 0) =', int(fonction2(0, 0)))
print('fonction2(0, 1) =', int(fonction2(0, 1)))
print('fonction2(1, 0) =', int(fonction2(1, 0)))
print('fonction2(1, 1) =', int(fonction2(1, 1)))
```

3\. Dans la partie "Définition des fonctions" on écrit :

```python linenums="1"
def fonction3(a, b):
    '''
    Fonction qui calcule la fonction 3.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, a)
    i2 = nand(b, b)
    s = nand(i1, i2)
    return s

def fonction4(a, b):
    '''
    Fonction qui calcule la fonction 4.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, b)
    i2 = nand(a, i1)
    i3 = nand(i1, b)
    s = nand(i2, i3)
    return s
```

et dans la partie "Programme principal" :

```python
print()

print('Table de vérité de la fonction 3 :')
print('fonction3(0, 0) =', int(fonction3(0, 0)))
print('fonction3(0, 1) =', int(fonction3(0, 1)))
print('fonction3(1, 0) =', int(fonction3(1, 0)))
print('fonction3(1, 1) =', int(fonction3(1, 1)))

print()

print('Table de vérité de la fonction 4 :')
print('fonction4(0, 0) =', int(fonction4(0, 0)))
print('fonction4(0, 1) =', int(fonction4(0, 1)))
print('fonction4(1, 0) =', int(fonction4(1, 0)))
print('fonction4(1, 1) =', int(fonction4(1, 1)))
```

# II. L’additionneur binaire

1\. On définit simplement la fonction avec ses commentaires dans la partie "Définition des fonctions".

```python
def addition(a, b, cin):
    '''
    Fonction qui calcule l'addition binaire de a et b en tenant compte de la retenue cin.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Entrée cin : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    Sortie cout : booléen, 0 ou 1, False ou True
    '''
    pass

```

2\. A la place de `pass` on écrit `i1 = a ^ b`.

3\. On ajoute à la suite `i2 = cin and i1`.

4\. Puis `i3 = b and a`.

5\. Puis `s = i1 ^ cin`.

6\. Puis `cout = i2 or i3`.

7\. Enfin `return (s, cout)`.

8\. C'est une table de vérité à $`2^3 = 8`$ entrées. Dans la partie "Programme principal" on écrit :

```python
print('Table de vérité de la fonction addition :')
print('addition(0, 0, 0) =', addition(0, 0, 0))
print('addition(0, 0, 1) =', addition(0, 0, 1))
print('addition(0, 1, 0) =', addition(0, 1, 0))
print('addition(0, 1, 1) =', addition(0, 1, 1))
print('addition(1, 0, 0) =', addition(1, 0, 0))
print('addition(1, 0, 1) =', addition(1, 0, 1))
print('addition(1, 1, 0) =', addition(1, 1, 0))
print('addition(1, 1, 1) =', addition(1, 1, 1))
```

# III. Application de l’additionneur à des entiers non signés

1\. On définit simplement la fonction avec ses commentaires dans la partie "Définition des fonctions".

```python
def add_entiers(entier_a, entier_b):
    '''
    Fonction qui calcule l'addition binaire de deux entiers.
    Entrée entier_a : chaîne de caractères
    Entrée entier_b : chaîne de caractères
    Sortie entier_s : chaîne de caractères
    '''
    pass

```

2\. La boucle permet de balayer les deux chaînes de caractères à l'envers. Si la longueur des deux chaînes est de 8, alors i prendra les 8 valeurs 7, 6, 5, ..., jusque 0.

3\. Dans la fonction on écrit à la place de `pass` :

```python
for i in reversed(range(len(entier_a))):
    a = int(entier_a[i])
    b = int(entier_b[i])
    print(a, b)
```

et dans le programme principal on teste la fonction avec par exemple :

```python
add_entiers('100110', '111010')
```

4\. Juste avant le `for` on écrit :

```python
entier_s = ''
cin = 0
```

5\. Dans la boucle à la place de `print(a, b)` :

```python
s, cout = addition(a, b, cin)
```

6\. Toujours dans la boucle, à la suite :

```python
entier_s = str(s) + entier_s
```

7\. Toujours dans la boucle, à la suite :

```python
cin = cout
```

8\. A la fin de la fonction, en dehors de la boucle on écrit :

```python
return entier_s
```

Pour le test, dans le programme principal on entre :

```python
print(add_entiers('0101', '0111'))
```
