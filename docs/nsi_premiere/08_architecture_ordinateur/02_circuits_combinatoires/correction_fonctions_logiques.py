#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Programme écrit par NOM PRENOM
Classe de NSI1 ou NSI2
'''


## Définition des fonctions

def nand(a, b):
    '''
    Fonction qui calcule la fonction NAND.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    s = not(a and b)
    return s

def fonction1(a):
    '''
    Fonction qui calcule la fonction 1.
    Entrée a : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    s = nand(a, a)
    return s

def fonction2(a, b):
    '''
    Fonction qui calcule la fonction 2.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, b)
    s = nand(i1, i1)
    return s

def fonction3(a, b):
    '''
    Fonction qui calcule la fonction 3.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, a)
    i2 = nand(b, b)
    s = nand(i1, i2)
    return s

def fonction4(a, b):
    '''
    Fonction qui calcule la fonction 4.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, b)
    i2 = nand(a, i1)
    i3 = nand(i1, b)
    s = nand(i2, i3)
    return s

## Programme principal

print('Table de vérité de la fonction NAND :')
print('nand(0, 0) =', int(nand(0, 0)))
print('nand(0, 1) =', int(nand(0, 1)))
print('nand(1, 0) =', int(nand(1, 0)))
print('nand(1, 1) =', int(nand(1, 1)))

print()

print('Table de vérité de la fonction 1 :')
print('fonction1(0) =', int(fonction1(0)))
print('fonction1(1) =', int(fonction1(1)))

print()

print('Table de vérité de la fonction 2 :')
print('fonction2(0, 0) =', int(fonction2(0, 0)))
print('fonction2(0, 1) =', int(fonction2(0, 1)))
print('fonction2(1, 0) =', int(fonction2(1, 0)))
print('fonction2(1, 1) =', int(fonction2(1, 1)))

print()

print('Table de vérité de la fonction 3 :')
print('fonction3(0, 0) =', int(fonction3(0, 0)))
print('fonction3(0, 1) =', int(fonction3(0, 1)))
print('fonction3(1, 0) =', int(fonction3(1, 0)))
print('fonction3(1, 1) =', int(fonction3(1, 1)))

print()

print('Table de vérité de la fonction 4 :')
print('fonction4(0, 0) =', int(fonction4(0, 0)))
print('fonction4(0, 1) =', int(fonction4(0, 1)))
print('fonction4(1, 0) =', int(fonction4(1, 0)))
print('fonction4(1, 1) =', int(fonction4(1, 1)))