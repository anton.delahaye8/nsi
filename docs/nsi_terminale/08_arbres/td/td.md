# **Structure de données : les arbres binaires**

## **Rappels**

Un arbre binaire **strict** est un arbre dont tous les nœuds internes possèdent exactement deux fils. (Autrement dit, un arbre binaire strict est un arbre dont chaque nœud possède zéro ou 2 fils. L'arbre vide n'est pas strict.)

Un arbre binaire **complet** est un arbre dans lequel toutes les feuilles sont à la même profondeur. (Il s'agit d'un arbre dont tous les niveaux sont remplis.)

Un arbre binaire **parfait** est un arbre dans lequel tous les niveaux sont remplis à l'exception éventuelle du dernier, et dans ce cas les feuilles du dernier niveau sont alignées à gauche.

Un arbre binaire **équilibré** est un arbre dont les deux fils sont des arbres équilibrés dont les hauteurs diffèrent d'au plus une unité.  Ainsi, l'accès à n'importe lequel des nœuds est en moyenne minimisé.

## **Quelques exercices**

![arbre_1](./arbre_1.png) ![arbre_2](./arbre_2.png) ![arbre_3](./arbre_3.png) ![arbre_4](./arbre_4.png)

### **Question 1**

Parmis les 4 Arbres ci dessus, lesquels sont **strict** ? **complet** ? **parfait** ? **équilibré** ? (compléter le tableau)

|type     |arbres|
|:-------:|:----:|
|strict   |      |
|complet  |      |
|parfait  |      |
|équilibré|      |

---

### **Question 2**

Compléter le tableau :

|arbres |taille|hauteur|Profondeur de 4|profondeur de 5|profondeur de 6|
|:-----:|:----:|:-----:|:-------------:|:-------------:|:-------------:|
|arbre 1|      |       |               |               |               |
|arbre 2|      |       |               |               |               |
|arbre 3|      |       |               |               |               |
|arbre 4|      |       |               |               |               |

---

### **Question 3**

Dessiner un arbre binaire :
- qui est **strict**, **équilibré** mais n'est pas **parfait**
- qui est **complet** mais pas **strict**
- qui est ni **strict** ni **complet** ni **parfait** ni **équilibré**.
