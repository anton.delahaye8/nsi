# **Structure de données : les arbres binaires**

## **Exercices d'applications**

**Exercice 1** - Donner le pseudo-code de la fonction suivante :

```python
def taille(arbre):
    if arbre.is_empty():
        return 0
    elif arbre.is_leaf():
        return 1
    else:
        return 1 + taille(arbre.get_left_subtree()) + taille(arbre.get_right_subtree())
```

---

**Exercice 2** - Transcrire le pseudo-code suivant en langage python :

```
parametre:
    (BinaryTree) une arbre binaire
return:
    (int) la hauteur de l'arbre
algorithme:
    si arbre est vide:
        return 0
    sinon si arbre est une feuille:
        return 1
    sinon:
        return 1 + maximum(hauteur(fils gauche), hauteur(fils droit))
```

---

**Exercice 3** - Définir une fonction ```profondeur(arbre, noeud)``` prenant en paramètre un arbre binaire et la valeur de l'étiquette du noeud que l'on recherche (on supposera que le noeud rechercher est bien dans l'arbre).

*note : pour la fonction ```profondeur```, on pourra prendre exemple du parcour de l'arbre de la fonction ```hauteur```.*