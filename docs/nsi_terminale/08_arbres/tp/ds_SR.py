from arbres_binary_tree import *

def afficher_B(a):
    if not a.is_empty():
        afficher_B(a.get_left_subtree())
        afficher_B(a.get_right_subtree())
        print(a.get_data(), end="")

Noeud_E = BinaryTree(1, BinaryTree(), BinaryTree())
Noeud_D = BinaryTree(0, BinaryTree(), BinaryTree())

Noeud_B = BinaryTree(2, Noeud_D, Noeud_E)
Noeud_C = BinaryTree(4, BinaryTree(), BinaryTree())

Noeud_A = BinaryTree(3, Noeud_B, Noeud_C)

arbre = Noeud_A

afficher_B(arbre)