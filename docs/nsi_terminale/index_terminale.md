# Ressources NSI - Classe de terminale


## Révisions - Algorithmes de base

Révision des algorithmes de base et de l'évaluation de leur complexité. Permet de reposer les bases, notamment le parcours de tableau et la manipulation de données.

- Diapo cours : [pdf](./01_revisions_algo/algo_revisions_cours_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_cours_diapo.odp)
- Diapo TD : [pdf](./01_revisions_algo/algo_revisions_TD_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_TD_diapo.odp)


## Structures de données - Modularité

On aborde la modularité tout en révisant la programmation Python, l'implémentation des algorithmes de base et l'évaluation de leurs performances.

- Diapo cours : [pdf](./02_modularite/Cours.pdf) ou [odp](./02_modularite/Cours.odp)
- Fiche TP : [pdf](./02_modularite/TP_modularite_algo.pdf) ou [odt](./02_modularite/TP_modularite_algo.odt)
- Modèles de fonction, programme et module Python : [zip](./02_modularite/exemples_python.zip)


## Langages et programmation - Récursivité

Approche en algorithmique et programmation. Introduction aux fractales.

- Diapo cours TD : [pdf](./03_recurrence/cours_recusivite.pdf) ou [odp](./03_recurrence/cours_recusivite.odp)
- Fiche TP : [pdf](./03_recurrence/TP_recurrence.pdf) ou [odt](./03_recurrence/TP_recurrence.odt)
- Fiche arbre d'appel : [pdf](./03_recurrence/arbre_appel.pdf) ou [odp](./03_recurrence/arbre_appel.odp)


## Structures de données - Programmation objet

On approfondit la structuration des données et programmes. Encapsulation.

- Diapo cours : [pdf](./04_programmation_objet/cours_objets.pdf) ou [odp](./04_programmation_objet/cours_objets.odp)
- Fiche TP : [pdf](./04_programmation_objet/TP_programmation_objet.pdf) ou [odt](./04_programmation_objet/TP_programmation_objet.odt)
- Modèle de classe : [zip](./04_programmation_objet/modele_classe_objet.zip)


## Structures de données - Type abstrait LISTE et listes chaînées

Le type abstrait LISTE et ses implémentations sont abordées à travers structures de données tableaux statiques et listes chainées. Les types abstraits et les structures de données qui les implémentes sont spécifiées par leur interface.

- Diapo cours : [pdf](./05_listes_chainees_piles_files/cours.pdf) ou [odp](./05_listes_chainees_piles_files/cours.odp)
- Fiche TP : [pdf](./05_listes_chainees_piles_files/TP_liste_chainees.pdf) ou [odt](./05_listes_chainees_piles_files/TP_liste_chainees.odt)


## Structures de données - Piles et Files

Les files et les piles sont implémentées grâce à des tableaux statiques ou des listes chaînées ce qui permet d'approfondir l'abstraction des structures de données abordée la semaine précédente.

- Diapo cours : [pdf](./05_listes_chainees_piles_files/cours.pdf) ou [odp](./05_listes_chainees_piles_files/cours.odp)
- Fiche TP : [pdf](./05_listes_chainees_piles_files/TP_piles_files.pdf) ou [odt](./05_listes_chainees_piles_files/TP_piles_files.odt)


## Architectures matérielles, OS et réseaux - Circuits intégrés

Depuis l'invention du circuit intégré par Texas Instrument, le microprocesseur n'a cessé d'évoluer vers plus de puissance grâce à la miniaturisation de ses composants, tout en limitant la consommation d'énergie.

- Diapo cours : [pdf](./06_circuits_integres/cours.pdf) ou [odp](./06_circuits_integres/cours.odp)


## Architectures matérielles, OS et réseaux - Gestion des processus et des ressources

- Résumé de cours : [md](./07_processus_ressources/cours.md)
- Diapo cours : [pdf](./07_processus_ressources/cours.pdf) ou [odp](./07_processus_ressources/cours.odp)
- Fiche TP sur la gestion des processus : [pdf](./07_processus_ressources/TP_processus.pdf) ou [odt](./07_processus_ressources/TP_processus.odt)
- Fiche TP sur la gestion des ressources : [pdf](./07_processus_ressources/TP_fork_interblocage.pdf) ou [odt](./07_processus_ressources/TP_fork_interblocage.odt)


## Structures de données et algorithmique - Arbres binaires, arbres binaires de recherche, autres structures arborescentes


- Résumé de cours : [md](./08_arbres/cours/arbres_binaires.md)
- Diapo cours : [pdf](./08_arbres/Diapo.pdf) ou [odp](./08_arbres/Diapo.odp)
- Fiche TD : [md](./08_arbres/td/td.md)
- Fiche TP 1 sur le parcours en profondeur : [md](./08_arbres/tp/partie1/parcours_profondeur.md)
- Fiche TP 2 sur le parcours en largeur : [md](./08_arbres/tp/partie2/parcours_largeur.md)
- Fiche TP 3 sur les arbres binaires de recherche : [md](./08_arbres/tp/partie3/ABR.md)
- Module à utiliser pour le TP : [zip](./08_arbres/tp/arbres_binary_tree.zip)
- Fiche exercices 1 : [pdf](./08_arbres/exos/exos.pdf) ou [md](./08_arbres/exos/exos.md)
- Fiche exercices 2 : [pdf](./08_arbres/exos/exo2.pdf) ou [md](./08_arbres/exos/exo2.md)

!!! note "Documents rédigés par :"
    **Decoster Timothée**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.


## Bases de données - Bases de données relationnelles

- Résumé de cours : [md](./09_bases_de_donnees/resume_cours_1/resume.md)
- Diapo de cours sur l'introduction aux bases de données : [pdf](./09_bases_de_donnees/diapo_V2.pdf) ou [odp](./09_bases_de_donnees/diapo_V2.odp)
- Fiche TD 1 sur le vocabulaire et les relations entre tables : [pdf](./09_bases_de_donnees/Exercices_bdr.pdf) ou [odt](./09_bases_de_donnees/Exercices_bdr.odt)
- Fiche TP 1 sur la création de tables : [pdf](./09_bases_de_donnees/tp_1.pdf) ou [odt](./09_bases_de_donnees/tp_1.odt)
- Base de données pour le TP 1 : [zip](./09_bases_de_donnees/lycee.zip)

- Diapo de cours sur le langage SQL : [pdf](./09_bases_de_donnees/cours_bd_1.pdf) ou [odp](./09_bases_de_donnees/cours_bd_1.odp)
- Fiche TD 2 sur le langage SQL : [pdf](./09_bases_de_donnees/TD_2.pdf) ou [odt](./09_bases_de_donnees/TD_2.odt)
- Base de donnée pour corriger le TD 2 : [zip](./09_bases_de_donnees/biblio.zip)
- Fiche TP 2 sur le langage SQL : [pdf](./09_bases_de_donnees/tp2.pdf) ou [odt](./09_bases_de_donnees/tp2.odt)
- Base de données pour le TP 2 : [zip](./09_bases_de_donnees/tp2.zip)
- Correction du TP 2 : [pdf](./09_bases_de_donnees/correction2TP.pdf) ou [odt](./09_bases_de_donnees/correction2TP.odt)
- Utilisation d'une base de données avec Python : [zip](./09_bases_de_donnees/biblio_python.zip)

- Fiche TD 3 sur les anomalies dans les bases de données relationnelles : [pdf](./09_bases_de_donnees/anomalies.pdf)
- Correction de l'exercice 6 : [pdf](./09_bases_de_donnees/correction_exercice_6.pdf) ou [odt](./09_bases_de_donnees/correction_exercice_6.odt)

!!! note "Documents rédigés par :"
    **Marette Amélie**, étudiante master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 1ère année, à l'Université de Lille.<br />
    **Ikli Youness**, étudiant master **M**étiers de l' **E**nseignement, de l' **É**ducation et de la **F**ormation, mention second degré NSI, 2ème année, à l'Université de Lille.


## Projets de Noël

Proposition de cinq projets à réaliser pendant les vacances de Noël :

- ASCII Art
- Simulateur de processeur
- Gestion des repas d’un restaurant scolaire
- Automate cellulaire
- Planète WA-TOR

- Diapo présentant les projets : [pdf](./10_projets_de_noel/projets_de_noel.pdf) ou [odp](./10_projets_de_noel/projets_de_noel.odp)


## Langages et programmation - Paradigmes et programmation fonctionnelle, impérative, objet

Tout comme le paradigme impératif découle naturellement de l'approche du calcul par Alan Turing (machine de Turing), le paradigme fonctionnel découle naturellement de l'approche du calcul par Alonzo Churh (&lambda;-calcul). Les principaux paradigmes, du plus déclaratif au plus impératif sont présentés avant d'introduire les principes fondamentaux de la programmation fonctionnelle.

- Diapo de cours : [pdf](./11_programmation_fonctionnelle/diapo_cours.pdf) ou [odp](./11_programmation_fonctionnelle/diapo_cours.odp)
- Fiche TD introduisant les principes fondamentaux : [pdf](./11_programmation_fonctionnelle/TP_prog_fonctionnelle.pdf) ou [odt](./11_programmation_fonctionnelle/TP_prog_fonctionnelle.odt)
- Base du programme turtle utilisé dans le TD : [zip](./11_programmation_fonctionnelle/turtle_fonctionnelle.zip)


## Architectures matérielles, OS et réseaux - Protocoles de routage

Internet est l'assemblage de réseaux autonomes. Après leur présentation et un rappel des notions élémentaires, le cours étudie les deux protocoles permettant d'inscrire dans les tables de routage les chemins que doivent emprunter les paquets de données dans ce type de réseau : **R**outing **I**nformation **P**rotocol et **O**pen **S**hortest **P**ath **F**irst.

- Diapo de cours : [pdf](./12_protocoles_reseaux/cours.pdf) ou [odp](./12_protocoles_reseaux/cours.odp)
- Algorithme de Dijkstra, application sur le graphe : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.odp)
- Algorithme de Dijkstra, application en tableau : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.odp)
- Fiche TD sur les protocole RIP et OSPF : [pdf](./12_protocoles_reseaux/TD_protocoles_reseaux.pdf) ou [odt](./12_protocoles_reseaux/TD_protocoles_reseaux.odt)
- Quelques topologies : [pdf](./12_protocoles_reseaux/topo_reseaux.pdf) ou [odp](./12_protocoles_reseaux/topo_reseaux.odp)
- TP illustrant le protocole RIP en Python : [pdf](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.pdf) ou [odt](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.odt)

!!! note "TP protocole RIP rédigé par :"
    **De Wancker Randy**, élève **N**umérique et **S**ciences **I**nfomatiques, terminale, lycée Raymond Queneau de Villeneuve d'Ascq.


## Algorithmique - Diviser pour régner et Programmation dynamique

- Diapo de cours : [pdf](./13_techniques_programmation/cours.pdf) ou [odp](./13_techniques_programmation/cours.odp)
- Fiche TD sur les techniques de programmation : [pdf](./13_techniques_programmation/tp_techniques_programmation.pdf) ou [odt](./13_techniques_programmation/tp_techniques_programmation.odt)
- Illustration de la recherche dichotomique récursive : [pdf](./13_techniques_programmation/SR_08_dichotomie_recursive.pdf) ou [odp](./13_techniques_programmation/SR_08_dichotomie_recursive.odp)
- Illustration de la recherche dichotomique non récursive : [pdf](../nsi_premiere/10_algorithmique/02_tris/SR_07_dichotomie_non_recursive.pdf) ou [odp](../nsi_premiere/10_algorithmique/02_tris/SR_07_dichotomie_non_recursive.odp)
- Illustration de la fusion non récursive : [pdf](./13_techniques_programmation/SR_04_fusion_non_recursive.pdf) ou [odp](./13_techniques_programmation/SR_04_fusion_non_recursive.odp)
- Illustration du tri fusion : [pdf](./13_techniques_programmation/SR_06_tri_fusion.pdf) ou [odp](./13_techniques_programmation/SR_06_tri_fusion.odp)
- Illustration de la fusion récursive : [pdf](./13_techniques_programmation/SR_05_fusion_recursive.pdf) ou [odp](./13_techniques_programmation/SR_05_fusion_recursive.odp)

## Langages et programmation - Mise au point des programmes

## Architectures matérielles, OS et réseaux - Sécurisation des communications
## Structures de données et algorithmique - Graphes
## Algorithmique - Parcours en profondeur et en largeur

## Langages et programmation - Calculabilité, programme en tant que donnée
## Algorithmique - Recherche textuelle
