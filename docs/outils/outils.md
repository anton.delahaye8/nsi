# Ressources NSI - Outils


## Logiciels

Le tableau ci-dessous regroupe quelques logiciels utiles pour apprendre la NSI et leur méthode d'installation pour les trois systèmes d'exploitation classiques.

<table>
    <tr>
        <td></td>
        <td align="center"><b>Windows</b></td>
        <td align="center"><b>Linux</b></td>
        <td align="center"><b>MacOS</b></td>
    </tr>
    <tr>
        <td>Langage de programmation <a href="https://www.python.org/">Python</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
    </tr>
    <tr>
        <td>Editeur de programmes Python <a href="https://thonny.org/">Thonny</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
    </tr>
    <tr>
        <td>Emulateur Linux <a href="https://www.cygwin.com/">Cygwin</a></td>
        <td align="center"><a href="./logiciels/cygwin.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Système de gestion de base de données <a href="https://www.sqlite.org/index.html">SQLite</a></td>
        <td align="center"><a href="./logiciels/sqlite-tools-windows.zip">à décompresser</a></td>
        <td align="center"><a href="./logiciels/sqlite-tools-linux.zip">à décompresser</a></td>
        <td align="center"><a href="./logiciels/sqlite-tools-osx.zip">à décompresser</a></td>
    </tr>
</table>


## Modules python

Modules Python d'intérêt et activités pour les prendre en main.

* L'interface graphique **Tkinter** : [pdf](./interfaces_graphiques/interfaces_graphiques_introduction.pdf) ou [odt](./interfaces_graphiques/interfaces_graphiques_introduction.odt)
* Animation avec une interface graphique **Tkinter** : [pdf](./interfaces_graphiques/tkinter_animation.pdf) ou [odt](./interfaces_graphiques/tkinter_animation.odt)
